package com.flutura.SystemDataExtracts;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import java.util.Properties;
import com.opencsv.CSVWriter;

import org.apache.commons.io.FileUtils;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemDataExtracts {

    private static final Logger log = LoggerFactory.getLogger(SystemDataExtracts.class);

    public static void main(String args[]) {

        int result;
        String year = args[0];
        String month = args[1];
        String fileUploadID = args[3];
        String systemDataType = args[2];


        SystemDataExtracts sde = new SystemDataExtracts();
        result = sde.connectToExternalDatabase(year,month,systemDataType,fileUploadID);

        if(result == 0) {
            System.exit(0);
        }
        else {
            System.exit(result);
        }
    }

    private int connectToExternalDatabase(String year, String month, String systemDataType, String fileUploadID) {

        int result = 0;
        Properties properties = new Properties();
        String configFileName = "application.properties";

        try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Properties file " + configFileName + " not found in classpath");
            }

            if(systemDataType.contains("CMR") || systemDataType.contains("Tour")) {
                result = connectToMSSQLServer(properties,systemDataType,year,month,fileUploadID);
            }
            else {
                result = connectToImpalaServer(properties,systemDataType,year,month,fileUploadID);
            }
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in connectToExternalDatabase: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in connectToExternalDatabase: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in connectToExternalDatabase: " + e.getMessage(),e);
        }
        return result;
    }

    private int connectToMSSQLServer(Properties properties, String systemDataType, String year, String month,
                                     String fileUploadID) {

        int loadResult;
        int finalResult = 0;
        String username;
        String password;
        String connectionURL;
        Connection connection = null;


        username = properties.getProperty("mssql.datasource.username");
        password = properties.getProperty("mssql.datasource.password");
        connectionURL = properties.getProperty("mssql.datasource.jdbcUrl");

        try {
            switch (systemDataType) {
                case "CMR":
                    connectionURL = connectionURL + "databaseName=PASON_CMR_DB";
                    connection = DriverManager.getConnection(connectionURL, username, password);
                    if (connection != null) {
                        System.out.println("Connected to SQL Server");
                        loadResult = extractCMR(properties, connection, year, month, fileUploadID);
                        if (loadResult != 0) {
                            System.out.println("Extracted CMR");
                            loadResult = extractCMRWalk(properties, connection, year, month, fileUploadID);
                            if (loadResult != 0) {
                                System.out.println("Extracted CMR Walks");
                                loadResult = extractCMRWellTime(properties, connection, year, month, fileUploadID);
                                if (loadResult != 0) {
                                    System.out.println("Extracted CMR WellTime");
                                    loadResult = extractCMRDetail(properties, connection, year, month, fileUploadID);
                                    if (loadResult != 0) {
                                        System.out.println("Extracted CMR Details");
                                        finalResult = 1;
                                        System.out.println("Loaded all CMR data sucessfully");
                                    } else throw new Exception("Loading CMRDetails failed");
                                } else throw new Exception("Loading CMRWellTime failed");
                            } else throw new Exception("Loading CMRWalk failed");
                        } else throw new Exception("Loading CMR failed");
                        System.out.println("Connected to MS SQL Server");
                    } else {
                        throw new Exception("Connection to MS SQL Server Failed");
                    }
                    break;
                case "CMRErrors":
                    connectionURL = connectionURL + "databaseName=PASON_CMR_DB";
                    connection = DriverManager.getConnection(connectionURL, username, password);
                    if (connection != null) {
                        loadResult = extractCMRError(properties, connection, year, month, fileUploadID);
                        if (loadResult != 0) {
                            finalResult = 1;
                            System.out.println("Loaded CMR Errors data sucessfully");
                        } else throw new Exception("Loading CMRErrors failed");
                    } else {
                        throw new Exception("Connection to MS SQL Server Failed");
                    }
                    break;
                case "Tour":
                    connectionURL = connectionURL + "databaseName=PASON_TOUR_DB";
                    connection = DriverManager.getConnection(connectionURL, username, password);
                    break;
                default:
                    throw new Exception("Invalid System data type");
            }
	        if(finalResult == 0) {
	            deactivateFileUploadFlag(connection,fileUploadID);
	        }
        } catch (SQLException e) {
            log.error("SQLException in connectToMSSQLServer: " + e.getMessage(),e);
            finalResult = 0;
        } catch (Exception e) {
            log.error("Exception in connectToMSSQLServer: " + e.getMessage(),e);
            finalResult = 0;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in connectToMSSQLServer: " + e.getMessage(),e);
            }
        }
        return finalResult;
    }

    private int connectToImpalaServer(Properties properties, String systemDataType, String year, String month,
                                       String fileUploadID) {

        int loadResult;
        int finalResult;
        String username;
        String password;
        String connectionURL;
        Connection connection = null;

        username = properties.getProperty("spring.datasource.username");
        password = properties.getProperty("spring.datasource.password");
        connectionURL = properties.getProperty("spring.datasource.jdbc-url");

        try {
            Class.forName("com.cloudera.impala.jdbc41.Driver");
            connectionURL = connectionURL + "ptendrilling";
            connection = DriverManager.getConnection(connectionURL,username,password);

            if(connection != null) {
                if(systemDataType.equals("Stands")) {
                    loadResult = extractStands(properties,connection,year,month,fileUploadID);
                    if(loadResult != 0) {
                        finalResult = 1;
                        System.out.println("Loaded Stands data sucessfully");
                    } else throw new Exception("Loading Stands data failed");
                }
                else if(systemDataType.equals("RMCT")) {
                    loadResult = extractRMCT(properties,connection,year,month,fileUploadID);
                    if(loadResult != 0) {
                        finalResult = 1;
                        System.out.println("Loaded RMCT data sucessfully");
                    } else throw new Exception("Loading RMCT data failed");
                }
                else if(systemDataType.equals("Walks")) {
                    loadResult = extractWalks(properties,connection,year,month,fileUploadID);
                    if(loadResult != 0) {
                        finalResult = 1;
                        System.out.println("Loaded Walks data sucessfully");
                    } else throw new Exception("Loading Walks data failed");
                }
                else {
                    throw new Exception("Invalid system data type");
                }
            }
            else {
                throw new Exception("Connection to Impala Server Failed");
            }

            if(finalResult == 0) {
                deactivateFileUploadFlag(connection,fileUploadID);
            }
        } catch (SQLException e) {
            log.error("SQLException in connectToImpalaServer: " + e.getMessage(),e);
            finalResult = 0;
        } catch (Exception e) {
            log.error("Exception in connectToImpalaServer: " + e.getMessage(),e);
            finalResult = 0;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in connectToImpalaServer: " + e.getMessage(),e);
            }
        }
        return finalResult;
    }

    private int extractCMR(Properties properties, Connection connection, String year, String month,
                            String fileUploadID) {

        int flag;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "CMR" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,-4);
        endDate = getNewDate(Date,cal,sdf,38);
        File file = new File(intermediateFilePath+fileName);

        try (Statement statement = connection.createStatement()){
            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');

            String query = "SELECT " + fileUploadID + " AS file_upload_id " +
                    ", cmr_data_id " +
                    ", report_date " +
                    ", well_num " +
                    ", well_name " +
                    ", operator_name " +
                    ", rig_name " +
                    ", previous_well_release_time " +
                    ", trucks_move_distance " +
                    ", rig_move_time " +
                    ", daywork_start_time " +
                    ", contractor_business_unit " +
                    ", 'false' AS exemption_flag " +
                    " FROM cmr_data_usalt7 " +
                    " WHERE report_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"CMRUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractCMR: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractCMR: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractCMR: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractCMR: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractCMR: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }

    private int extractCMRWalk(Properties properties, Connection connection, String year, String month,
                           String fileUploadID) {

        int flag = 0;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "CMRWalk" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,-4);
        endDate = getNewDate(Date,cal,sdf,38);
        File file = new File(intermediateFilePath+fileName);

        try(Statement statement = connection.createStatement()) {

            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');

            String query = "SELECT " + fileUploadID + " AS file_upload_id " +
                    " , cmr_walk_time_id " +
	                " , b.cmr_data_id " +
                    " , walk_time " +
                    ", 'false' AS exemption_flag " +
                    " FROM cmr_data_usalt7 a " +
                    " INNER JOIN cmr_walk_times_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    " WHERE report_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"CMRWalkUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractCMRWalk: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractCMRWalk: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractCMRWalk: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractCMRWalk: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractCMRWalk: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }

    private int extractCMRWellTime(Properties properties, Connection connection, String year, String month,
                           String fileUploadID) {

        int flag;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "CMRWellTime" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,-4);
        endDate = getNewDate(Date,cal,sdf,38);
        File file = new File(intermediateFilePath+fileName);


        try(Statement statement = connection.createStatement()) {

            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');

            String query = "SELECT " + fileUploadID + " AS file_upload_id " +
                    " , cmr_well_time_id " +
 	                " , b.cmr_data_id " +
                    " , b.start_time " +
                    " , b.end_time " +
                    ", 'false' AS exemption_flag " +
                    " FROM cmr_data_usalt7 a " +
                    " INNER JOIN cmr_well_times_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    " WHERE report_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"CMRWellTimeUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractCMRWellTime: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractCMRWellTime: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractCMRWellTime: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractCMRWellTime: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractCMRWellTime: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }

    private int extractCMRDetail(Properties properties, Connection connection, String year, String month,
                           String fileUploadID) {

        int flag;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "CMRDetail" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,-4);
        endDate = getNewDate(Date,cal,sdf,38);
        File file = new File(intermediateFilePath+fileName);

        try(Statement statement = connection.createStatement()) {
            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');

            String query = "SELECT " + fileUploadID + " AS file_upload_id " +
                    " , b.cmr_time_code_data_id " +
                    " , b.cmr_data_id " +
                    " , b.[from] AS from_timestamp " +
	                " , b.[to] AS to_timestamp " +
	                " , b.hours " +
                    " , b.code " +
                    " , b.sub_code " +
                    " , b.details " +
                    ", 'false' AS exemption_flag " +
                    " FROM cmr_data_usalt7 a " +
                    " INNER JOIN cmr_time_code_data_usalt7 b ON a.cmr_data_id = b.cmr_data_id " +
                    " WHERE report_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"CMRDetailUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractCMRDetail: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractCMRDetail: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractCMRDetail: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractCMRDetail: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractCMRDetail: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }

    private int extractCMRError(Properties properties, Connection connection, String year, String month,
                                 String fileUploadID) {

        int flag;
        int loadResult = 0;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "CMRError" + "_" + year + month + ".csv";
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);

        File file = new File(intermediateFilePath+fileName);

        try(Statement statement = connection.createStatement()) {

            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');

	        String query = "WITH dingus AS ( " +
                            "SELECT cmr_data_id " +
                            ", iadc_tour_record_id " +
                            ", error_code " +
                            ", MIN(validation_date) AS error_first_observed " +
                            ", MAX(validation_date) AS error_last_observed " +
                            "FROM cmr_error_code_log err " +
                            "GROUP BY cmr_data_id, iadc_tour_record_id, error_code " +
                            "), GroupUnNumbered AS ( " +
                            "SELECT " + fileUploadID + " AS file_upload_id " +
                            ", NULL AS cmr_error_index " +
                            ", rig_name " +
                            ", YEAR(validation_date) AS rig_year " +
                            ", MONTH(validation_date) AS rig_month " +
                            ", errors.cmr_data_id " +
                            ", report_date " +
                            ", d.error_first_observed " +
                            ", d.error_last_observed " +
                            ", MAX(errors.description) AS error_text " +
                            ", 'false' as exemption_flag " +
                            "FROM cmr_error_code_log errors " +
                            "INNER JOIN dingus d ON errors.cmr_data_id = d.cmr_data_id AND errors.error_code = d.error_code " +
                            "WHERE YEAR(validation_date ) = " + Integer.parseInt(year) +
                            "AND MONTH(validation_date) = " + Integer.parseInt(month) +
                            "AND DATEDIFF(DAY, d.error_first_observed, errors.validation_date) >= 3 " +
                            "AND errors.error_code <> '21017' " +
                            "AND NOT EXISTS ( " +
                            "SELECT NULL " +
                            "FROM cmr_validation_exclusion validations " +
                            "WHERE errors.well_num = validations.well_num " +
                            "AND errors.report_date BETWEEN validations.min_report_dat AND validations.max_report_dat " +
                            "AND errors.error_code = CASE WHEN validations.errors = 'All' THEN errors.error_code ELSE validations.errors END " +
                            ") " +
                            "GROUP BY rig_name, YEAR(validation_date), MONTH(validation_date), errors.cmr_data_id, report_date, d.error_first_observed, d.error_last_observed " +
                            "UNION " +
                            " SELECT " + fileUploadID + " AS file_upload_id " +
                            ", NULL AS cmr_error_index " +
                            ", tour.rig_num " +
                            ", YEAR(validation_date) AS rig_year " +
                            ", MONTH(validation_date) AS rig_month " +
                            ", errors.iadc_tour_record_id AS cmr_data_id " +
                            ", DATEADD(DAY, 1, tour.report_date) AS report_date " +
                            ", d.error_first_observed " +
                            ", d.error_last_observed " +
                            ", MAX(errors.description) AS error_text " +
                            ", 'false' as exemption_flag " +
                            "FROM cmr_error_code_log errors " +
                            "INNER JOIN dingus d ON errors.iadc_tour_record_id = d.iadc_tour_record_id AND errors.error_code = d.error_code " +
                            "INNER JOIN PASON_TOUR_DB.dbo.iadc_tour_record tour ON tour.iadc_tour_record_id = d.iadc_tour_record_id " +
                            "WHERE YEAR(validation_date ) = " + Integer.parseInt(year) +
                            "AND MONTH(validation_date) = " + Integer.parseInt(month) +
                            "AND DATEDIFF(DAY, d.error_first_observed, errors.validation_date) >= 3 " +
                            "AND errors.error_code = '21017' " +
                            "AND NOT EXISTS ( " +
                            "SELECT NULL " +
                            "FROM cmr_validation_exclusion validations " +
                            "WHERE errors.well_num = validations.well_num " +
                            "AND errors.report_date BETWEEN validations.min_report_dat AND validations.max_report_dat " +
                            "AND errors.error_code = CASE WHEN validations.errors = 'All' THEN errors.error_code ELSE validations.errors END " +
                            ") " +
                            "group by tour.rig_num, YEAR(validation_date), MONTH(validation_date), errors.iadc_tour_record_id, tour.report_date, d.error_first_observed, d.error_last_observed " +
                            ") " +
                            "SELECT gun.file_upload_id " +
                            ", ROW_NUMBER() OVER(ORDER BY gun.rig_name, gun.report_date, gun.error_first_observed) AS cmr_error_index " +
                            ", gun.rig_name " +
                            ", gun.rig_year " +
                            ", gun.rig_month " +
                            ", gun.cmr_data_id " +
                            ", gun.report_date " +
                            ", gun.error_first_observed " +
                            ", gun.error_last_observed " +
                            ", gun.error_text " +
			                ", gun.exemption_flag " +
                            "FROM GroupUnNumbered gun" ;
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"CMRErrorUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractCMRError: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractCMRError: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractCMRError: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractCMRError: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractCMRError: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }

    private int extractStands(Properties properties, Connection connection, String year, String month,
                               String fileUploadID) {

        int flag;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "Stands" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,0);
        endDate = getNewDate(Date,cal,sdf,30);
        File file = new File(intermediateFilePath+fileName);

        try(Statement statement = connection.createStatement()) {
            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');


            String query = "SELECT " + fileUploadID + " AS file_upload_id " +
                    ", ROW_NUMBER() OVER(ORDER BY ws.well_num, ws.stand_id) AS line_order" +
                    ", ws.well_num" +
                    ", ws.stand_id" +
                    ", to_timestamp(ws.stand_start_time) AS stand_start_time" +
                    ", to_timestamp(ws.stand_end_time) AS stand_end_time" +
                    ", ws.starting_hole_depth" +
                    ", ws.ending_hole_depth" +
                    ", ws.starting_bit_depth" +
                    ", ws.ending_bit_depth" +
                    ", ws.stand_length" +
                    ", ws.in_out" +
                    ", ws.footage_drilled" +
                    ", ws.avg_wob" +
                    ", ws.avg_hkl" +
                    ", ws.avg_rop" +
                    ", ws.avg_rpm" +
                    ", ws.avg_ptsr" +
                    ", ws.avg_spp" +
                    ", ws.avg_torque" +
                    ", ws.avg_flow AS avg_flow_in" +
                    ", ws.avg_flop AS avg_flow_out" +
                    ", ws.avg_difp AS avg_diff_pressure" +
                    ", ws.total_sec" +
                    ", ws.sec_rigging_up" +
                    ", ws.sec_nipple_up_down" +
                    ", ws.sec_test_bop" +
                    ", ws.sec_npt_repair_rig" +
                    ", ws.sec_waiting_on_cement" +
                    ", ws.sec_cut_drill_line" +
                    ", ws.sec_rotating" +
                    ", ws.sec_sliding" +
                    ", ws.sec_in_slips" +
                    ", ws.sec_reaming_in" +
                    ", ws.sec_reaming_out" +
                    ", ws.sec_washing_in" +
                    ", ws.sec_washing_out" +
                    ", ws.sec_running_casing" +
                    ", ws.sec_trip_in" +
                    ", ws.sec_trip_out" +
                    ", ws.sec_circulating" +
                    ", ws.sec_circulating_while_rotating" +
                    ", ws.sec_other" +
                    ", NULL AS sec_static" +
                    ", ws.sec_cementing" +
                    ", NULL AS sec_out_of_hole" +
                    ", ws.total_ft_per_hour AS total_ft_per_hr" +
                    ", ws.blocks_moving_ft_per_hr AS block_moving_ft_per_hr" +
                    ", ws.tour" +
                    ", ws.sec_w2s" +
                    ", ws.sec_s2s" +
                    ", ws.sec_s2w" +
                    ", ws.sec_w2s_reaming AS sec_reaming_w2s" +
                    ", ws.s2w_pump_recycles AS pump_cycles" +
                    ", ws.rotary_footage AS rotating_footage" +
                    ", ws.slide_footage AS sliding_footage" +
                    ", ws.rotary_rop AS rotating_rop" +
                    ", ws.slide_rop AS sliding_rop" +
                    ", ws.stand_type" +
                    ", ws.casing_section" +
                    ", ws.casing_size" +
                    ", ws.hole_status" +
                    ", ws.bit_in_hole_section" +
                    ", ws.hole_progress_section" +
                    ", ws.casing_progress_section" +
                    ", ws.crew" +
                    ", '2' AS version" +
                    ", wd.rig_name" +
                    ", 'false' as exemption_flag" +
                    " FROM ptendrilling.ptendrilling_wellstands ws" +
                    " INNER JOIN ptendrilling.ptendrilling_welldata wd ON ws.well_num = wd.well_num" +
                    " WHERE ws.stand_start_time BETWEEN unix_timestamp('" + startDate  + "') " +
                    " AND unix_timestamp('" + endDate + "')" ;
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"StandsUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractStands: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractStands: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractStands: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractStands: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractStands: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }

    private int extractRMCT(Properties properties, Connection connection, String year, String month,
                              String fileUploadID) {

        int flag;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "RMCT" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,0);
        endDate = getNewDate(Date,cal,sdf,30);
        File file = new File(intermediateFilePath+fileName);

        try(Statement statement = connection.createStatement()) {

            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');

            String query = "WITH Moves AS (" +
                    " SELECT a.rig_name" +
                    " , a.previous_well_release_time" +
                    " , a.daywork_start_time" +
                    " , a.trucks_move_distance" +
                    " , a.rig_move_time / 24.0 AS RIG_MOVE_DAYS" +
                    " , rt.display_text AS rig_type" +
                    " FROM pten_cmr.cmr_data_usalt7 a" +
                    " INNER JOIN pten_cmr.rig r ON r.display_text = a.rig_name" +
                    " INNER JOIN pten_cmr.rigtype rt ON r.rig_type_id = rt.rig_type_id" +
                    " WHERE a.previous_well_release_time BETWEEN unix_timestamp('" + startDate + "') " +
                    " AND unix_timestamp('" + endDate + "')" +
                    " AND a.trucks_move_distance IS NOT NULL" +
                    " AND a.daywork_start_time IS NOT NULL" +
                    " AND a.previous_well_release_time IS NOT NULL" +
                    " GROUP BY a.rig_name, a.previous_well_release_time, a.daywork_start_time, " +
                    " a.trucks_move_distance, a.rig_move_time / 24.0, rt.display_text" +
                    ")" +
                    " SELECT " + fileUploadID + " AS file_upload_id " +
                    " , ROW_NUMBER() OVER(ORDER BY m.rig_name) AS line_order" +
                    " , m.rig_name" +
                    " , ROW_NUMBER() OVER(PARTITION BY m.rig_name ORDER BY m.daywork_start_time) AS move_ordinal" +
                    " , to_timestamp(m.previous_well_release_time) AS move_start" +
                    " , to_timestamp(m.daywork_start_time) AS move_end" +
                    " , m.trucks_move_distance" +
                    " , m.rig_move_days" +
                    " , '' as target_time" +
                    " , '' as partial_credit_time" +
                    " , 'false' as exemption_flag" +
                    " FROM Moves m" +
                    " ORDER BY 1,2" ;
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"RMCTUpload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractRMCT: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractRMCT: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractRMCT: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractRMCT: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractRMCT: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }


    private int extractWalks(Properties properties, Connection connection, String year, String month,
                              String fileUploadID) {

        int flag;
        int loadResult = 0;
        Date endDate;
        Date startDate;

        Connection conn = null;
        ResultSet result;
        CSVWriter writer = null;
        FileWriter fileWriter = null;
        String newDestination;

        Calendar cal = Calendar.getInstance();
        String Date = year + "-" + month + "-" + "01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String fileName = "Walks" + "_" + year + month + ".csv";
        String destinationPath = properties.getProperty("file.destination");
        File destinationDirectory = new File(destinationPath);
        String intermediateFilePath = properties.getProperty("file.intermediateServerPath");

        startDate = getNewDate(Date,cal,sdf,0);
        endDate = getNewDate(Date,cal,sdf,30);
        File file = new File(intermediateFilePath+fileName);

        try(Statement statement = connection.createStatement()) {
            fileWriter = new FileWriter(file);
            writer = new CSVWriter(fileWriter,'|','"');


            String query = "SELECT " + fileUploadID + " AS file_upload_id " +
                    ", a.rig_name " +
                    ", a.phase_number AS new_walk_ordinal " +
                    ", a.well_num " +
                    ", UPPER(w.well_name) AS well_name " +
                    ", a.start_time " +
                    ", to_timestamp(CAST(unix_timestamp(a.start_time) + (a.walk_time * 3600) AS BIGINT)) AS end_time " +
                    ", a.walk_time AS overall_walk_time " +
                    ", SUM(b.hours) AS f_walk_time " +
                    ", CASE WHEN COALESCE(SUM(b.hours), 0.0) = 0.0 THEN a.walk_time ELSE SUM(b.hours) END AS math_walk_time " +
                    ", 4.0 AS target_time " +
                    ", 4.5 AS partial_credit_time " +
                    ", CAST(NULL AS DECIMAL) AS walk_distance " +
                    ", false AS exemption_flag " +
                    "FROM pten_cmr.vw_well_phases a " +
                    "INNER JOIN pten_cmr.vw_all_details b ON a.well_num = b.well_num AND unix_timestamp(b.from_timestamp) BETWEEN unix_timestamp(a.start_time) AND unix_timestamp(a.start_time) + (a.walk_time * 3600) - 1 AND code = '1' AND sub_code IN ('.F','.F.01','.F.02') " +
                    "INNER JOIN pten_cmr.well w ON a.well_num = w.well_num " +
                    "WHERE a.start_time BETWEEN '" + startDate + "' AND '" + endDate + "' " +
                    "AND walk_time IS NOT NULL " +
                    "GROUP BY a.rig_name " +
                    ", a.phase_number " +
                    ", a.well_num " +
                    ", UPPER(w.well_name) " +
                    ", a.start_time " +
                    ", a.walk_time";
            result = statement.executeQuery(query);
            writer.writeAll(result,true,false,false);
            writer.flush();
            if (destinationDirectory.exists()) {
                newDestination = directoryCheck(destinationPath,year);
                newDestination = directoryCheck(newDestination,month);
            }
            else {
                boolean res = destinationDirectory.mkdir();
                if (res) {
                    System.out.println("Creation successful");
                    newDestination = directoryCheck(destinationPath,year);
                    newDestination = directoryCheck(newDestination,month);
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }

            FileUtils.copyFile(file,new File(newDestination+"/"+file.getName()));

            int res1 = fileUploadStatus(file,newDestination);
            int res2 = fileUploadStatus(file,intermediateFilePath);

            if(res1 != 0 && res2 != 0) {
                conn = connectToPostgres(properties);
                if(conn != null) {
                    flag = loadCSVToPostgres(conn,file,"walkupload");
                    if(flag != 0) {
                        loadResult = 1;
                        file.delete();
                        System.out.println("Data loaded to postgres");
                    }
                    else {
                        throw new Exception("Data not loaded to postgres");
                    }
                }
                System.out.println("File upload successful");
            }
            else {
                throw new Exception("File upload unsuccessful");
            }
        } catch (SQLException e) {
            log.error("SQLException in extractWalks: " + e.getMessage(),e);
        } catch (IOException e) {
            log.error("IOException in extractWalks: " + e.getMessage(),e);
        } catch (Exception e) {
            log.error("Exception in extractWalks: " + e.getMessage(),e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (SQLException e) {
                log.error("SQLException in extractWalks: " + e.getMessage(),e);
            } catch (IOException e) {
                log.error("IOException in extractWalks: " + e.getMessage(),e);
            }
        }
        return loadResult;
    }


    private Date getNewDate(String Date, Calendar cal, SimpleDateFormat sdf, int noOfDays) {

        String newDate = null;

        try {
            cal.setTime(sdf.parse(Date));
            cal.add(Calendar.DAY_OF_MONTH,noOfDays);
            newDate = sdf.format(cal.getTime());
        } catch (ParseException e) {
            log.error("ParseException in getNewDate: " + e.getMessage(),e);
        }
        return java.sql.Date.valueOf(newDate);
    }

    private String directoryCheck(String destination, String directoryName) {

        String path =  null;
        File directory = null;

        try {
            directory = new File(destination+"/"+directoryName);
            if(directory.exists()) {
                path = directory.getAbsolutePath();
            }
            else {
                boolean res = directory.mkdir();
                if(res) {
                    path = directory.getAbsolutePath();
                }
                else {
                    throw new Exception("Directory creation unsuccessful");
                }
            }
        } catch (Exception e) {
            log.error("Exception in directoryCheck: " + e.getMessage(),e);
        }
        return path;
    }

    private int fileUploadStatus(File file, String destination) {

        int flag = 0;

        try {
            File destinationDir = new File(destination);
            for(File files : Objects.requireNonNull(destinationDir.listFiles())) {
                if(files.getName().equals(file.getName())) {
                    flag = 1;
                    break;
                }
            }
        } catch (Exception e) {
            log.error("Exception in fileUploadStatus: " + e.getMessage(),e);
        }
        return flag;
    }

    private int loadCSVToPostgres(Connection connection, File file, String tableName) {

        int flag = 0;

        try (FileReader fileReader = new FileReader(file)) {

            CopyManager copyManager = new CopyManager((BaseConnection) connection);
            copyManager.copyIn("COPY " + tableName + " FROM STDIN WITH DELIMITER '|' CSV HEADER", fileReader);
            flag = 1;

            System.err.println("Done.");
        } catch (Exception e) {
            log.error("Exception in loadCSVToPostgres: " + e.getMessage(),e);
        }
        return flag;
    }

    private Connection connectToPostgres(Properties properties) {

        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(properties.getProperty("ptenInternal.datasource.scorecard.jdbc-url"),
                    properties.getProperty("ptenInternal.datasource.scorecard.username"),
                    properties.getProperty("ptenInternal.datasource.scorecard.password"));
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            log.error("SQLException in connectToPostgres: " + e.getMessage(),e);
        } catch (ClassNotFoundException e) {
            log.error("ClassNotFoundException in connectToPostgres: " + e.getMessage(),e);
        }
        return conn;
    }

    private void deactivateFileUploadFlag(Connection conn, String fileUploadID) {

        try {

            Statement statement = conn.createStatement();
            String query = "UPDATE FileUpload set active = false WHERE file_upload_id = " + fileUploadID;

            statement.executeUpdate(query);
            statement.close();
        } catch (SQLException e) {
            log.error("SQLException in deactivateFileUploadFlag: " + e.getMessage(),e);
        }
    }
}
